################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

################################################################################
# Dockerfile for building an image that can be used to build the VOXL open
# source Kernel.
# Author: Rahul Anand
################################################################################
FROM --platform=linux/amd64 ubuntu:xenial
ENV DEBIAN_FRONTEND noninteractive

# Add repo for the right gcc
RUN apt-get update \
   && apt-get -y --quiet --no-install-recommends install software-properties-common

RUN add-apt-repository ppa:ubuntu-toolchain-r/test
RUN add-apt-repository ppa:deadsnakes/ppa

# Now install the packages
RUN apt-get update \
   && apt-get -y --quiet --no-install-recommends install \
      gcc-4.8-multilib \
      g++-4.8-multilib \
      gawk \
      wget \
      git-core \
      diffstat \
      unzip \
      texinfo \
      gcc-multilib \
      build-essential \
      chrpath \
      bzip2 \
      zip \
      cpio \
      bc \
      make \
      autoconf \
      automake \
      libtool \
      wget \
      curl \
      sudo \
      libxml-simple-perl \
      doxygen \
      qemu-user-static \
      android-tools-fsutils \
      debootstrap \
      schroot \
      vim \
      less \
      iputils-ping \
      python \
      python3-pip \
      python-setuptools \
      python-tk \
      openssh-server \
      gnupg-agent \
      zlib1g-dev \
   && apt-get clean autoclean \
   && pip3 install --upgrade pip \
   && curl https://storage.googleapis.com/git-repo-downloads/repo-1 > /usr/bin/repo \
   && chmod a+x /usr/bin/repo \
   && echo "dash dash/sh boolean false" | debconf-set-selections \
   && DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash

#    python3.6 \
# install python3.6 from source, since it's no longer available for download for xenial
ADD https://www.python.org/ftp/python/3.6.9/Python-3.6.9.tgz /opt/install/
RUN cd /opt/install/ && tar -vxf Python-3.6.9.tgz && cd Python-3.6.9 && ./configure && make -j 8 altinstall && rm -rf /opt/install

#use previously downloaded Python-3.6.9.tgz
#ADD Python-3.6.9.tgz /opt/install/
#RUN cd /opt/install/Python-3.6.9 && ./configure && make -j 8 altinstall

# Add a non-root user with the following details:
# username: user
# password: user
# Home dir: /home/user
# Groups   : sudo, user

RUN /usr/sbin/useradd -m -s /bin/bash -G sudo user \
  && echo user:user | /usr/sbin/chpasswd

# add the build script
ADD build.sh /home/user
ADD *.xml /home/user

# set defualt user to user when launch docker
USER user
